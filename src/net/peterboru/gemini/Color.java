package net.peterboru.gemini;

/**
 * Color class to represent colors on a 0-1 scale (good for OpenGL integration).
 * @author Peter Boru
 */
public class Color {

    public static final Color BLACK = new Color(0, 0, 0, 0);
    public static final Color WHITE = new Color(1, 1, 1, 1);

    private float r, g, b, a;

    public Color() {
        r = 0;
        g = 0;
        b = 0;
        a = 0;
    }

    public Color(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public void setColor(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    public float getR() {
        return r;
    }

    public void setR(float r) {
        this.r = r;
    }

    public float getG() {
        return g;
    }

    public void setG(float g) {
        this.g = g;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    public String toString() {
        return "{" + r + ", " + g + ", " + b + ", " + a + "}";
    }
}
