package net.peterboru.gemini;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.opengl.GLContext;

import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

/**
 * The core class of Gemini. In charge of starting up GLFW and LWGJL and the main loop. Also the central timing
 * system is embedded into the main loop.
 * @author Peter Boru
 */
public class Engine {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    public static final int DEFAULT_WIDTH = 640;
    public static final int DEFAULT_HEIGHT = 480;

    private static boolean isRunning;
    private static double delta;
    private static int fps;

    private GLFWErrorCallback errorCallback;
    private GLFWKeyCallback keyCallback;

    private long window;

    /**
     * Starts up the entire engine, handles main loop startup and stop procedures.
     */
    public void start() {
        logger.log(Level.INFO, "Initiating Gemini...");
        isRunning = true;

        try {
            glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));

            if (glfwInit() != GL_TRUE)
                throw new IllegalStateException("Unable to initialize GLFW");

            glfwDefaultWindowHints();
            glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
            glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

            window = glfwCreateWindow(DEFAULT_WIDTH, DEFAULT_HEIGHT, "Game", NULL, NULL);
            if (window == NULL)
                throw new RuntimeException("Failed to create a window");

            glfwSetKeyCallback(window, keyCallback = new GLFWKeyCallback() {
                @Override
                public void invoke(long window, int key, int scancode, int action, int mods) {
                    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                        glfwSetWindowShouldClose(window, GL_TRUE);
                }
            });

            ByteBuffer videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            glfwSetWindowPos(window,
                    (GLFWvidmode.width(videoMode) - DEFAULT_WIDTH) / 2,
                    (GLFWvidmode.height(videoMode) - DEFAULT_HEIGHT) / 2
            );

            glfwMakeContextCurrent(window);
            glfwSwapInterval(1);
            glfwShowWindow(window);

            mainLoop();
        } finally {
            glfwTerminate();
            errorCallback.release();
        }

        logger.log(Level.INFO, "Gemini initiated.");
    }

    private void stop() {
        logger.log(Level.INFO, "Terminating Gemini...");
        isRunning = false;

        // ENGINE STOP CODE

        logger.log(Level.INFO, "Gemini terminated.");
    }

    private void mainLoop() {

        double currentTime;
        double lastTime = System.currentTimeMillis();

        double logTimer = 0;

        int frames = 0;

        GLContext.createFromCurrent();

        glClearColor(0, 0, 0, 0);

        while (isRunning) {
            // TIMING CODE
            currentTime = System.currentTimeMillis();
            delta = currentTime - lastTime;

            logTimer += delta;

            frames += 1;

            if (logTimer >= 1000) {
                logger.log(Level.INFO, "Update: " + delta + "ms " + frames + "fps");

                fps = frames;
                frames = 0;
                logTimer = 0;
            }

            if (glfwWindowShouldClose(window) == GL_TRUE)
                isRunning = false;

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glfwSwapBuffers(window);
            glfwPollEvents();

            lastTime = currentTime;
        }

        stop();
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static double getDelta() {
        return  delta;
    }

    public static int getFPS() {
        return fps;
    }
}
