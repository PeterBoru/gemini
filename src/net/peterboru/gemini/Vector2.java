package net.peterboru.gemini;

/**
 * Standard 2-dimensional vector with math functions.
 * @author Peter Boru
 */
public class Vector2 {

    private double x, y;

    public Vector2() {
        setX(0);
        setY(0);
    }

    public Vector2(double x, double y) {
        setX(x);
        setY(y);
    }

    public Vector2(Vector2 vec) {
        x = vec.getX();
        y = vec.getY();
    }

    public Vector2 normalize() {
        Vector2 normalizedVector = new Vector2();

        normalizedVector.setX(x / magnitude());
        normalizedVector.setY(y / magnitude());

        return normalizedVector;
    }

    public double magnitude() {
        return Math.sqrt(x * x + y * y);
    }

    public void add(double x, double y) {
        this.x += x;
        this.y += y;
    }

    public void add(Vector2 vec) {
        this.x += vec.getX();
        this.y += vec.getY();
    }

    public void sub(double x, double y) {
        this.x -= x;
        this.y -= y;
    }

    public void sub(Vector2 vec) {
        this.x -= vec.getX();
        this.y -= vec.getY();
    }

    public void mul(double x, double y) {
        this.x *= x;
        this.y *= y;
    }

    public void mul(Vector2 vec) {
        this.x *= vec.getX();
        this.y *= vec.getY();
    }

    public void div(double x, double y) {
        this.x /= x;
        this.y /= y;
    }

    public void div(Vector2 vec) {
        this.x /= vec.getX();
        this.y /= vec.getY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
