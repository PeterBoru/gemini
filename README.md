# README #

Game Development Framework

### What is this repository for? ###

Gemini is a small-scale 2D game development framework built on LWJGL. It is designed to have a minimalist base while achieving high extensibility.

### Dependencies ###

* [LWJGL 2.9.3](http://www.lwjgl.org/)
* [Slick-Util](http://slick.ninjacave.com/slick-util/)